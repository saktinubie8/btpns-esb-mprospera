package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoanRefreshRequest extends BaseRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private Loan[] loanList;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Loan[] getLoanList() {
		return loanList;
	}

	public void setLoanList(Loan[] loanList) {
		this.loanList = loanList;
	}

	@Override
	public String toString() {
		return "RefreshLoanRequest [username=" + username + ", imei=" + imei + ", loanList=" + Arrays.toString(loanList)
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}

}
