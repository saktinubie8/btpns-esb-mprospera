package id.co.sigma.mx.endpoint.mpro01.request;

import java.util.Arrays;

public class CustomerNonPrs {

	private String customerId;
	private LoanCustomerNonPrs[] loanList;
	private SavingNonPrs[] savingList;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public LoanCustomerNonPrs[] getLoanList() {
		return loanList;
	}

	public void setLoanList(LoanCustomerNonPrs[] loanList) {
		this.loanList = loanList;
	}

	public SavingNonPrs[] getSavingList() {
		return savingList;
	}

	public void setSavingList(SavingNonPrs[] savingList) {
		this.savingList = savingList;
	}

	@Override
	public String toString() {
		return "CustomerNonPrs [customerId=" + customerId + ", loanList=" + Arrays.toString(loanList) + ", savingList="
				+ Arrays.toString(savingList) + "]";
	}

}
