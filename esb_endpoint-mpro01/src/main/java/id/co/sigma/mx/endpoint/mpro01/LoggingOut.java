package id.co.sigma.mx.endpoint.mpro01;

import java.util.Date;
import java.util.Properties;

import id.co.sigma.mx.message.util.ConversionListener;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageUtils;
import org.apache.cxf.message.XMLMessage;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingOut extends LoggingOutInterceptor {

	private static final transient Logger LOGGER = LoggerFactory
			.getLogger(LoggingOut.class);
	
	private MessageHolder messageHolder;
	private ConversionListener messageLogWriter;
	private String endpoint;

	
	public void setMessageHolder(MessageHolder messageHolder) {
		this.messageHolder = messageHolder;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public LoggingOut()
	{
		super("pre-stream");
		super.setPrettyLogging(true);
	}

	public void setMessageLogWriter(ConversionListener messageLogWriter) {
		this.messageLogWriter = messageLogWriter;
	}

	@Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {
    	Message message = PhaseInterceptorChain.getCurrentMessage();
    	String id = "";
    	
    	if(message instanceof XMLMessage)
    	{
    		XMLMessage xml = (XMLMessage) message;
    		id = ""+xml.getExchange().getInMessage().get("org.apache.cxf.interceptor.LoggingMessage.ID");
    	}
    	else if(message instanceof SoapMessage)
    	{
    		SoapMessage soap = (SoapMessage) message;
    		id = ""+soap.getExchange().getInMessage().get("org.apache.cxf.interceptor.LoggingMessage.ID");
    	}
    	    	
    	if(messageHolder.getMessageList().get(id)!=null)
    	{
    		if(!(messageHolder.getMessageList().get(id) instanceof String))
    		{
	    		MyMessage emsg = (MyMessage)messageHolder.getMessageList().remove(id);
		    	emsg.setRawMessage(loggingMessage.toString());
		    	
		    	boolean requestor = MessageUtils.isRequestor(message);
		    	boolean outbound = MessageUtils.isOutbound(message);
		
		    	if (requestor) {
		    	    if (!outbound) {
		    	    	//This is your RESPONSE message from WS Server
		    	    	emsg.setResponse();
		    	    }
		    	    else
		    	    {
		    	    	//This is your REQUEST message to WS Server
		    	    }
		    	} else {
		    	    if (!outbound) {
		    	    	//This is your REQUEST message from client
		    	    	emsg.setRequest();
		    	    }
		    	    else
		    	    {
		    	    	//This is your RESPONSE message to client
		    	    	emsg.setResponse();
		    			Properties prop = new Properties();
		    			prop.putAll(emsg.getValueHolder());
		    			messageLogWriter.saveMessageLogs(emsg.getTransactionId(), new Date(), endpoint, emsg.getChannelMappingCode(), emsg.isRequest(), 
		    					emsg.getRawMessage(),prop);
		    	    }
		    	}	    		        
    		}
    		else
    		{
    			//memastikan tidak ada record di message holder yang tertinggal (bisa menyebabkan heap full)
    			messageHolder.getMessageList().remove(id);
    		}
    	}
        return super.formatLoggingMessage(loggingMessage);
    }

}
