package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tokenRequest")
public class TokenRequest extends BaseRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private String authType;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	@Override
	public String toString() {
		return "TokenRequest [username=" + username + ", imei=" + imei + ", getTransmissionDateAndTime()="
				+ getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
				+ "]";
	}
}
