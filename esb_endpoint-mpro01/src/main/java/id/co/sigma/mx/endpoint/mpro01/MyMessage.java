package id.co.sigma.mx.endpoint.mpro01;

import id.co.sigma.mx.message.ExternalMessageAdapter;

import java.util.Map;
/**
 *
 * @author <a href="mailto:muhammad.ichsan@sigma.co.id">Muhammad Ichsan</a>
 *
 */
public class MyMessage extends ExternalMessageAdapter {
	private static final long serialVersionUID = 1939407992804146258L;

	private String assocKey;
	private String rawMessage;
	private String transactionId;
	private String cxfId;
	private String channelMappingCode;
	
	public String getChannelMappingCode() {
		return channelMappingCode;
	}

	public void setChannelMappingCode(String channelMappingCode) {
		this.channelMappingCode = channelMappingCode;
	}

	public String getCxfId() {
		return cxfId;
	}

	public void setCxfId(String cxfId) {
		this.cxfId = cxfId;
	}

	public String getRawMessage() {
		return rawMessage;
	}

	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String getValue(String field) {
		if(super.getValue(field)!=null)
			return super.getValue(field);
		return "";
	}

	public MyMessage() {

	}

	// TODO: Check if this method is required. If data is not as complex as SOA
	// Webservice, then you don't need this.
	public String getServiceName() {
		return getValue("$service_name");
	}
	
	// Mandatory method
	@Override
	public boolean isRequest() {
		if("RQ".equalsIgnoreCase(this.getValue("mti")))
			return true;
		return false;
	}
	
	// Mandatory method
	@Override
	public String getAssocKey() {
		return assocKey;
	}

	public void setResponse() {
		setValue("mti", "RS");
	}

	public void setRequest() {
		setValue("mti", "RQ");
	}
	
	public void setRequest(boolean isRequest) {
		if(isRequest)
			setValue("mti", "RQ");
		else
			setValue("mti", "RS");
	}
	
	@Override
	public String toString() {
		return "WS Message assocKey="+this.assocKey+" [valueHolder=" + this.valueHolder + "]";
	}
	
	void setAssocKey(String assocKey) {
		this.assocKey = assocKey;
	}
	
	public void setValueHolder(Map<String,String> data)
	{
		this.valueHolder.putAll(data);
	}
	
	@Override
	public String getResponseCode() {
		// TODO Auto-generated method stub
		return getValue("responseCode");
	}
	
	@Override
	public String getMappingCode() {
		// TODO Auto-generated method stub
		return getValue("mappingCode");
	}
}
