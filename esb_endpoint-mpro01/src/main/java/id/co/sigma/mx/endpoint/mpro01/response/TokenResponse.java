package id.co.sigma.mx.endpoint.mpro01.response;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tokenResponse")
public class TokenResponse extends BaseResponse {
	private String tokenChallenge;

	public String getTokenChallenge() {
		return tokenChallenge;
	}

	public void setTokenChallenge(String tokenChallenge) {
		this.tokenChallenge = tokenChallenge;
	}

	@Override
	public String toString() {
		return "TokenResponse [tokenChallenge=" + tokenChallenge + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
