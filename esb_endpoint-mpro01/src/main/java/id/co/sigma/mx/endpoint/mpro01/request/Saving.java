package id.co.sigma.mx.endpoint.mpro01.request;

public class Saving {

	private String accountId;
	private String accountNumber;
	private String withdrawalAmount;
	private String nextWithdrawalAmountPlan;
	private String isCloseSavingAccount;
	private String clearBalance;
	private String depositAmount;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getWithdrawalAmount() {
		if(withdrawalAmount==null)
			return "";
		return withdrawalAmount;
	}

	public void setWithdrawalAmount(String withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	public String getNextWithdrawalAmountPlan() {
		if(nextWithdrawalAmountPlan==null)
			return "";
		return nextWithdrawalAmountPlan;
	}

	public void setNextWithdrawalAmountPlan(String nextWithdrawalAmountPlan) {
		this.nextWithdrawalAmountPlan = nextWithdrawalAmountPlan;
	}

	public String getIsCloseSavingAccount() {
		return isCloseSavingAccount;
	}

	public void setIsCloseSavingAccount(String isCloseSavingAccount) {
		this.isCloseSavingAccount = isCloseSavingAccount;
	}

	public String getClearBalance() {
		if(clearBalance==null)
			return "";
		return clearBalance;
	}

	public void setClearBalance(String clearBalance) {
		this.clearBalance = clearBalance;
	}

	public String getDepositAmount() {
		if(depositAmount==null)
			return "";
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	@Override
	public String toString() {
		return "Saving [accountId=" + accountId + ", accountNumber=" + accountNumber + ", withdrawalAmount="
				+ withdrawalAmount + ", nextWithdrawalAmountPlan=" + nextWithdrawalAmountPlan
				+ ", isCloseSavingAccount=" + isCloseSavingAccount + ", clearBalance=" + clearBalance
				+ ", depositAmount=" + depositAmount + "]";
	}

}
