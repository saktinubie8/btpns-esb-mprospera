package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubmitNonPrsRequest extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private CustomerNonPrs[] customerList;
	private String tokenChallenge;
	private String tokenResponse;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public CustomerNonPrs[] getCustomerList() {
		return customerList;
	}

	public void setCustomerList(CustomerNonPrs[] customerList) {
		this.customerList = customerList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTokenChallenge() {
		if(tokenChallenge==null)
			return "";
		return tokenChallenge;
	}

	public void setTokenChallenge(String tokenChallenge) {
		this.tokenChallenge = tokenChallenge;
	}

	public String getTokenResponse() {
		if(tokenResponse==null)
			return "";
		return tokenResponse;
	}

	public void setTokenResponse(String tokenResponse) {
		this.tokenResponse = tokenResponse;
	}

	@Override
	public String toString() {
		return "SubmitNonPrsRequest [username=" + username + ", imei=" + imei + ", customerList="
				+ Arrays.toString(customerList) + ", tokenChallenge=" + tokenChallenge + ", tokenResponse="
				+ tokenResponse + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}
	
}
