package id.co.sigma.mx.endpoint.mpro01;

import id.co.sigma.mx.message.ExternalMessageFactory;
import id.co.sigma.mx.message.SimpleExternalMessageFactory;
import id.co.sigma.mx.message.util.ConversionListener;
import id.co.sigma.mx.stan.TransactionIdGenerator;
import id.co.sigma.mx.transport.MessageListener;
import id.co.sigma.mx.transport.Transport;
import id.co.sigma.mx.transport.TransportNotReadyException;
import id.co.sigma.mx.transport.commons.monitoring.EndpointStatusManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class MyTransport implements Transport<MyMessage>, InitializingBean, DisposableBean {

	private static final transient Logger LOGGER = LoggerFactory.getLogger(MyTransport.class);
	private List<MessageListener<MyMessage>> listeners;
	private final ExternalMessageFactory<MyMessage> externalMessageFactory;
	private EndpointStatusManager endpointStatusManager;
	private String endpointCode;
	private final Map<String, CountDownLatch> latches = new ConcurrentHashMap<String, CountDownLatch>();
	private final Map<String, MyMessage> responses = new ConcurrentHashMap<String, MyMessage>();
	private MessageHolder messageHolder;
	private ConversionListener messageLogWriter;

	public void setMessageLogWriter(ConversionListener messageLogWriter) {
		this.messageLogWriter = messageLogWriter;
	}

	public void setMessageHolder(MessageHolder messageHolder) {
		this.messageHolder = messageHolder;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	private static AtomicLong lastCounter = new AtomicLong();
	long timeout = 60L;

	public MyTransport() {
		listeners = new ArrayList<MessageListener<MyMessage>>();
		externalMessageFactory = new SimpleExternalMessageFactory<MyMessage>(MyMessage.class);
	}

	public MyMessage sendMessage(String mappingCode, Map<String, String> reqData) {
		String errorMessage = "GENERAL ERROR";
		MyMessage requestMessage = externalMessageFactory.getInstance();
		String assoc = nextUniqueAssoc();
		requestMessage.setValueHolder(reqData);
		requestMessage.setAssocKey(assoc);
		requestMessage.setValue("mappingCode", mappingCode);
		requestMessage.setRawMessage(reqData.get("rawMessage"));
		requestMessage.setTransactionId(TransactionIdGenerator.generateTransactionId(endpointCode,
				"" + endpointStatusManager.getMxNodeId(), new Date()));
		requestMessage.setCxfId(reqData.get("cxfId"));
		requestMessage.setChannelMappingCode(mappingCode);
		requestMessage.setRequest();
		CountDownLatch latch = new CountDownLatch(1);
		latches.put(assoc, latch);

		Boolean hasError = false;
		Properties prop = new Properties();
		try {
			prop.putAll(requestMessage.getValueHolder());
			messageLogWriter.saveMessageLogs(requestMessage.getTransactionId(), new Date(), endpointCode,
					requestMessage.getMappingCode(), requestMessage.isRequest(), requestMessage.getRawMessage(), prop);
		}catch(NullPointerException e) {
			messageLogWriter.saveMessageLogs(requestMessage.getTransactionId(), new Date(), endpointCode,
					requestMessage.getMappingCode(), requestMessage.isRequest(), requestMessage.getRawMessage(), new Properties());
			hasError = true;
		}

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Notify Core MX for incoming message from Webservice AssocKey=" + assoc);
		tellListeners(requestMessage);

		try {
			latch.await(timeout, TimeUnit.SECONDS);
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}

		MyMessage response = responses.remove(assoc);
		latches.remove(assoc);

		if (response != null) {
			response.setTransactionId(requestMessage.getTransactionId());
			response.setChannelMappingCode(requestMessage.getChannelMappingCode());
			if (LOGGER.isDebugEnabled())
				LOGGER.debug("Sending WS Response=" + response.toString());
		}else {
			response = requestMessage;
			response.setValue("responseCode", "XX");
			response.setValue("responseMessage", errorMessage);
			if (LOGGER.isDebugEnabled())
				LOGGER.debug("Sending WS Response");
		}
		messageHolder.getMessageList().put(requestMessage.getCxfId(), response);
		return response;
	}

	public void write(MyMessage message) {
		if (!endpointStatusManager.readStatus(endpointCode).equals("ready")) {
			throw new TransportNotReadyException("Socket connection is not established.");
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Receiving message from MX");
		}

		responses.put(message.getAssocKey(), message);
		latches.remove(message.getAssocKey()).countDown();
	}

	/**
	 * Telling listeners that an external message is coming into MX
	 * 
	 * @param response
	 */
	private void tellListeners(MyMessage response) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Sending WS Message to MX:" + response.toString());
		}

		for (MessageListener<MyMessage> listener : listeners) {
			listener.messageReceived(response);
		}
	}

	public void addMessageListener(MessageListener<MyMessage> listener) {
		listeners.add(listener);
	}

	private String nextUniqueAssoc() {
		return FastDateFormat.getInstance("ddMMHHmmss").format(System.currentTimeMillis())
				+ lastCounter.getAndIncrement();
	}

	public void destroy() throws Exception {
		LOGGER.info("Stopping Endpoint " + endpointCode);
		this.endpointStatusManager.updateStatus(endpointCode, "disconnected");
		this.endpointStatusManager.createNetworkLog(endpointCode, EndpointStatusManager.STATUS_BUNDLE_STOP,
				"Bundle " + endpointCode + " stopped");
		LOGGER.info("Endpoint " + endpointCode + " successfully stopped");
	}

	public void afterPropertiesSet() throws Exception {
		LOGGER.info("Starting Endpoint " + endpointCode);
		this.endpointStatusManager.updateStatus(endpointCode, "ready");
		this.endpointStatusManager.createNetworkLog(endpointCode, EndpointStatusManager.STATUS_BUNDLE_START,
				"Bundle " + endpointCode + " started");
		LOGGER.info("Endpoint " + endpointCode + " successfully started");
	}

	public void setEndpointCode(String endpointCode) {
		this.endpointCode = endpointCode;
	}

	public void setEndpointStatusManager(EndpointStatusManager endpointStatusManager) {
		this.endpointStatusManager = endpointStatusManager;
	}

	public void setMessageListeners(List<MessageListener<MyMessage>> listeners) {
		this.listeners = listeners;
	}
}
