package id.co.sigma.mx.endpoint.mpro01.request;

import java.util.Arrays;

public class Customer {

	private String customerId;
	private String isAttend;
	private String notAttendReason;
	private LoanCustomer[] loanList;
	private Saving[] savingList;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getIsAttend() {
		return isAttend;
	}

	public void setIsAttend(String isAttend) {
		this.isAttend = isAttend;
	}

	public String getNotAttendReason() {
		if(notAttendReason==null)
			return "";
		return notAttendReason;
	}

	public void setNotAttendReason(String notAttendReason) {
		this.notAttendReason = notAttendReason;
	}

	public LoanCustomer[] getLoanList() {
		return loanList;
	}

	public void setLoanList(LoanCustomer[] loanList) {
		this.loanList = loanList;
	}

	public Saving[] getSavingList() {
		return savingList;
	}

	public void setSavingList(Saving[] savingList) {
		this.savingList = savingList;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", isAttend=" + isAttend + ", notAttendReason=" + notAttendReason
				+ ", loanList=" + Arrays.toString(loanList) + ", savingList=" + Arrays.toString(savingList) + "]";
	}
	
}
