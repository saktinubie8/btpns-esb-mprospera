package id.co.sigma.mx.endpoint.mpro01.request;

public class LoanCustomer {

	private String loanId;
	private String appid;
	private String disbursementFlag;
	private String installmentPaymentAmount;
	private String useEmergencyFund;
	private String isEarlyTermination;
	private String earlyTerminationReason;
	private String marginDiscountDeviationFlag;
	private String marginDiscountDeviationAmount;
	private String marginDiscountDeviationPercentage;

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getDisbursementFlag() {
		return disbursementFlag;
	}

	public void setDisbursementFlag(String disbursementFlag) {
		this.disbursementFlag = disbursementFlag;
	}

	public String getInstallmentPaymentAmount() {
		if(installmentPaymentAmount==null)
			return "";
		return installmentPaymentAmount;
	}

	public void setInstallmentPaymentAmount(String installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}

	public String getUseEmergencyFund() {
		if(useEmergencyFund==null)
			return "";
		return useEmergencyFund;
	}

	public void setUseEmergencyFund(String useEmergencyFund) {
		this.useEmergencyFund = useEmergencyFund;
	}

	public String getIsEarlyTermination() {
		if(isEarlyTermination==null)
			return "";
		return isEarlyTermination;
	}

	public void setIsEarlyTermination(String isEarlyTermination) {
		this.isEarlyTermination = isEarlyTermination;
	}

	public String getEarlyTerminationReason() {
		if(earlyTerminationReason==null)
			return "";
		return earlyTerminationReason;
	}

	public void setEarlyTerminationReason(String earlyTerminationReason) {
		this.earlyTerminationReason = earlyTerminationReason;
	}

	public String getMarginDiscountDeviationFlag() {
		return marginDiscountDeviationFlag;
	}

	public void setMarginDiscountDeviationFlag(String marginDiscountDeviationFlag) {
		this.marginDiscountDeviationFlag = marginDiscountDeviationFlag;
	}

	public String getMarginDiscountDeviationAmount() {
		if(marginDiscountDeviationAmount==null)
			return "";
		return marginDiscountDeviationAmount;
	}

	public void setMarginDiscountDeviationAmount(String marginDiscountDeviationAmount) {
		this.marginDiscountDeviationAmount = marginDiscountDeviationAmount;
	}

	public String getMarginDiscountDeviationPercentage() {
		if(marginDiscountDeviationPercentage==null)
			return "";
		return marginDiscountDeviationPercentage;
	}

	public void setMarginDiscountDeviationPercentage(String marginDiscountDeviationPercentage) {
		this.marginDiscountDeviationPercentage = marginDiscountDeviationPercentage;
	}

	@Override
	public String toString() {
		return "LoanCustomer [loanId=" + loanId + ", appid=" + appid + ", disbursementFlag=" + disbursementFlag
				+ ", installmentPaymentAmount=" + installmentPaymentAmount + ", useEmergencyFund=" + useEmergencyFund
				+ ", isEarlyTermination=" + isEarlyTermination + ", earlyTerminationReason=" + earlyTerminationReason
				+ ", marginDiscountDeviationFlag=" + marginDiscountDeviationFlag + ", marginDiscountDeviationAmount="
				+ marginDiscountDeviationAmount + ", marginDiscountDeviationPercentage="
				+ marginDiscountDeviationPercentage + "]";
	}

}
