package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubmitPrsRequest extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private String psCompanion;
	private String prsId;
	private String sentraId;
	private String prsDate;
	private Customer[] customerList;
	private String tokenChallenge;
	private String tokenResponse;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getPsCompanion() {
		return psCompanion;
	}

	public void setPsCompanion(String psCompanion) {
		this.psCompanion = psCompanion;
	}

	public String getPrsId() {
		return prsId;
	}

	public void setPrsId(String prsId) {
		this.prsId = prsId;
	}

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getPrsDate() {
		return prsDate;
	}

	public void setPrsDate(String prsDate) {
		this.prsDate = prsDate;
	}

	public Customer[] getCustomerList() {
		return customerList;
	}

	public void setCustomerList(Customer[] customerList) {
		this.customerList = customerList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTokenChallenge() {
		if(tokenChallenge==null)
			return "";
		return tokenChallenge;
	}

	public void setTokenChallenge(String tokenChallenge) {
		this.tokenChallenge = tokenChallenge;
	}

	public String getTokenResponse() {
		if(tokenResponse==null)
			return "";
		return tokenResponse;
	}

	public void setTokenResponse(String tokenResponse) {
		this.tokenResponse = tokenResponse;
	}

	@Override
	public String toString() {
		return "SubmitPrsRequest [username=" + username + ", imei=" + imei + ", psCompanion=" + psCompanion + ", prsId="
				+ prsId + ", sentraId=" + sentraId + ", prsDate=" + prsDate + ", customerList="
				+ Arrays.toString(customerList) + ", tokenChallenge=" + tokenChallenge + ", tokenResponse="
				+ tokenResponse + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}

}
