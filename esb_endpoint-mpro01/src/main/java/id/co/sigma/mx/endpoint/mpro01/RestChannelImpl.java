package id.co.sigma.mx.endpoint.mpro01;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import id.co.sigma.mx.endpoint.mpro01.request.AssignPsRequest;
import id.co.sigma.mx.endpoint.mpro01.request.Customer;
import id.co.sigma.mx.endpoint.mpro01.request.CustomerNonPrs;
import id.co.sigma.mx.endpoint.mpro01.request.CustomerRequest;
import id.co.sigma.mx.endpoint.mpro01.request.DeviationRequest;
import id.co.sigma.mx.endpoint.mpro01.request.GroupRequest;
import id.co.sigma.mx.endpoint.mpro01.request.InquiryPrsRequest;
import id.co.sigma.mx.endpoint.mpro01.request.InsuranceRequest;
import id.co.sigma.mx.endpoint.mpro01.request.LoanCustomer;
import id.co.sigma.mx.endpoint.mpro01.request.LoanCustomerNonPrs;
import id.co.sigma.mx.endpoint.mpro01.request.LoanRequest;
import id.co.sigma.mx.endpoint.mpro01.request.LoginRequest;
import id.co.sigma.mx.endpoint.mpro01.request.PasswordChangeRequest;
import id.co.sigma.mx.endpoint.mpro01.request.PasswordResetRequest;
import id.co.sigma.mx.endpoint.mpro01.request.LoanRefreshRequest;
import id.co.sigma.mx.endpoint.mpro01.request.Saving;
import id.co.sigma.mx.endpoint.mpro01.request.SavingNonPrs;
import id.co.sigma.mx.endpoint.mpro01.request.SentraPS;
import id.co.sigma.mx.endpoint.mpro01.request.SentraRequest;
import id.co.sigma.mx.endpoint.mpro01.request.SubmitNonPrsRequest;
import id.co.sigma.mx.endpoint.mpro01.request.SubmitPrsRequest;
import id.co.sigma.mx.endpoint.mpro01.request.SurveyRequest;
import id.co.sigma.mx.endpoint.mpro01.request.TokenRequest;
import id.co.sigma.mx.endpoint.mpro01.request.TrxInquiryRequest;
import id.co.sigma.mx.endpoint.mpro01.response.AssignPsResponse;
import id.co.sigma.mx.endpoint.mpro01.response.CustomerInquiry;
import id.co.sigma.mx.endpoint.mpro01.response.CustomerResponse;
import id.co.sigma.mx.endpoint.mpro01.response.DeviationResponse;
import id.co.sigma.mx.endpoint.mpro01.response.EchoResponse;
import id.co.sigma.mx.endpoint.mpro01.response.GroupResponse;
import id.co.sigma.mx.endpoint.mpro01.response.InquiryPrsResponse;
import id.co.sigma.mx.endpoint.mpro01.response.InsuranceResponse;
import id.co.sigma.mx.endpoint.mpro01.response.Loan;
import id.co.sigma.mx.endpoint.mpro01.response.LoanResponse;
import id.co.sigma.mx.endpoint.mpro01.response.LoginResponse;
import id.co.sigma.mx.endpoint.mpro01.response.PasswordChangeResponse;
import id.co.sigma.mx.endpoint.mpro01.response.PasswordResetResponse;
import id.co.sigma.mx.endpoint.mpro01.response.LoanRefreshResponse;
import id.co.sigma.mx.endpoint.mpro01.response.SentraResponse;
import id.co.sigma.mx.endpoint.mpro01.response.SubmitNonPrsResponse;
import id.co.sigma.mx.endpoint.mpro01.response.SubmitPrsResponse;
import id.co.sigma.mx.endpoint.mpro01.response.SurveyResponse;
import id.co.sigma.mx.endpoint.mpro01.response.TokenResponse;
import id.co.sigma.mx.endpoint.mpro01.response.TrxHistory;
import id.co.sigma.mx.endpoint.mpro01.response.TrxInquiryResponse;

@WebService
public class RestChannelImpl {
    private static final transient Logger LOGGER = LoggerFactory.getLogger(RestChannelImpl.class);

    private MyTransport communicator;
    private MessageHolder messageHolder;
    private String listAllowedIp;
    private List<String> nullList;

    public Boolean ipIsValid() {
        if (listAllowedIp == null || listAllowedIp.equals("") || listAllowedIp.equals("*"))
            return true;
        HttpServletRequest request = (HttpServletRequest) PhaseInterceptorChain.getCurrentMessage()
                .get(AbstractHTTPDestination.HTTP_REQUEST);
        String clientIp = request.getHeader("X-FORWARDED-FOR");
        if (clientIp == null) {
            clientIp = request.getRemoteAddr();
        }
        String[] ipArray = listAllowedIp.split(",");
        for (String ip : ipArray) {
            LOGGER.trace("Client IP : " + clientIp);
            if ((ip.contains("*") && clientIp.startsWith(ip.substring(0, ip.length() - 2))) || ip.equals(clientIp))
                return true;
        }
        return false;
    }

    @POST
    @Path("/echo")
    @Produces({MediaType.APPLICATION_JSON})
    public EchoResponse echo() {
        EchoResponse response = new EchoResponse();
        if (ipIsValid())
            response.setResponseCode("00");
        else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public LoginResponse login(LoginRequest request) {

        LoginResponse response = new LoginResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("password", request.getPassword());
            req.put("imei", request.getImei());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());

            Map<String, String> res = sendToESB("PRO_LOGIN_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            response.setOfficeId(res.get("officeId"));
            response.setState(res.get("state"));
            List<String> roles = new ArrayList<String>();
            int count = 0;
            if (res.get("role.count") != null)
                count = Integer.parseInt(res.get("role.count"));
            for (int i = 0; i < count; i++) {
                roles.add(res.get("role-" + i));
            }
            response.setRole(roles.toArray(new String[0]));
            List<String> centers = new ArrayList<String>();
            count = 0;
            if (res.get("center.count") != null)
                count = Integer.parseInt(res.get("center.count"));
            for (int i = 0; i < count; i++) {
                centers.add(res.get("center-" + i));
            }
            response.setCenters(centers.toArray(new String[0]));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/sentra/{action}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SentraResponse addSentra(SentraRequest request, @PathParam("action") String action) {

        SentraResponse response = new SentraResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("sentraName", request.getSentraName());
            req.put("sentraAddress", request.getSentraAddress());
            req.put("kecamatan", request.getKecamatan());
            req.put("kelurahan", request.getKelurahan());
            req.put("rtrw", request.getRtRw());
            req.put("postcode", request.getPostcode());
            req.put("prsDay", request.getPrsDay());
            req.put("startedDate", request.getStartedDate());
            req.put("meetingPlace", request.getMeetingPlace());
            req.put("centerCodeOfficer", request.getUsername());
            req.put("officeCode", request.getOfficeCode());
            req.put("action", request.getAction());
            req.put("recurAfter", request.getRecurAfter());
            req.put("dayNumber", request.getDayNumber());
            req.put("reffId", request.getReffId());
            req.put("provinsi", request.getProvinsi());
            req.put("kabupaten", request.getKabupaten());
            req.put("customerNumberLeader", request.getCustomerNumberLeader());
            req.put("mode", action);
            req.put("actionTime", request.getActionTime());

            Map<String, String> res = sendToESB("PRO_ADD_SENTRA_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            response.setSentraId(res.get("sentraId"));
            response.setKodeSentra(res.get("kodeSentra"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/group/{action}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public GroupResponse addGroup(GroupRequest request, @PathParam("action") String action) {

        GroupResponse response = new GroupResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("trainedDate", request.getTrainedDate());
            req.put("groupName", request.getGroupName());
            req.put("customerNumberLeader", request.getGroupLeader());
            req.put("centerCodeOfficer", request.getUsername());
            req.put("customerNumberCenter", request.getCustomerNumberCenter());
            req.put("action", request.getAction());
            req.put("reffId", request.getReffId());
            req.put("mode", action);

            Map<String, String> res = sendToESB("PRO_ADD_GROUP_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            response.setGroupId(res.get("groupId"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/tokenRequest")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public TokenResponse tokenRequest(TokenRequest request) {

        TokenResponse response = new TokenResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("authType", request.getAuthType());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());

            Map<String, String> res = sendToESB("PRO_TOKEN_REQ_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            if (response.getResponseCode().equals("00"))
                response.setTokenChallenge(res.get("tokenChallenge"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/resetPassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public PasswordResetResponse resetPassword(PasswordResetRequest request) {

        PasswordResetResponse response = new PasswordResetResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("oldPassword", request.getOldPassword());
            req.put("newPassword", request.getNewPassword());
            req.put("tokenChallenge", request.getTokenChallenge());
            req.put("tokenResponse", request.getTokenResponse());

            Map<String, String> res = sendToESB("PRO_PASS_RESET_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/changePassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public PasswordChangeResponse changePassword(PasswordChangeRequest request) {

        PasswordChangeResponse response = new PasswordChangeResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("oldPassword", request.getOldPassword());
            req.put("newPassword", request.getNewPassword());

            Map<String, String> res = sendToESB("PRO_PASS_CHANGE_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/addCustomer")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public CustomerResponse addCustomer(CustomerRequest request) {

        CustomerResponse response = new CustomerResponse();
        if (ipIsValid()) {

            nullList = new ArrayList<String>();
            Map<String, String> req = processRequestMap();
            req.put("username", mandatory("username", request.getUsername()));
            req.put("imei", mandatory("imei", request.getImei()));
            req.put("transmissionDateAndTime", mandatory("transmissionDateAndTime", request.getTransmissionDateAndTime()));
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("customerNumberCenter", mandatory("customerNumberCenter", request.getCustomerNumberCenter()));
            req.put("customerNumberGroup", mandatory("customerNumberGroup", request.getCustomerNumberGroup()));
            req.put("actionFlag", mandatory("actionFlag", request.getActionFlag()));
            req.put("reffId", mandatory("reffId", request.getReffId()));
            req.put("createdBy", mandatory("createdBy", request.getCreatedBy()));
            req.put("createdDate", mandatory("createdDate", request.getCreatedDate()));
            req.put("groupFlag", mandatory("groupFlag", request.getGroupFlag()));
            req.put("fullName", mandatory("fullName", request.getFullName()));
            req.put("firstName", mandatory("firstName", request.getFirstName()));
            req.put("nickName", mandatory("nickName", request.getNickName()));
            req.put("governmentId", mandatory("governmentId", request.getGovernmentId()));
            req.put("dateOfGovernmentId", mandatory("dateOfGovernmentId", request.getDateOfGovernmentId()));
            req.put("dateOfBirth", mandatory("dateOfBirth", request.getDateOfBirth()));
            req.put("placeOfBirth", mandatory("placeOfBirth", request.getPlaceOfBirth()));
            req.put("telephone", request.getTelephone());
            req.put("motherName", mandatory("motherName", request.getMotherName()));
            req.put("gender", mandatory("gender", request.getGender()));
            req.put("maritalStatus", mandatory("maritalStatus", request.getMaritalStatus()));
            req.put("occupation", mandatory("occupation", request.getOccupation()));
            req.put("numberOfChildren", request.getNumberOfChildren());
            req.put("businessIncomeFamily", request.getBusinessIncomeFamily());
            req.put("citizenShip", request.getCitizenShip());
            req.put("religion", request.getReligion());
            req.put("ethinicity", request.getEthinicity());
            req.put("citizenType", request.getCitizenType());
            req.put("residenceStatus", request.getResidenceStatus());
            req.put("latestEducation", mandatory("latestEducation", request.getLatestEducation()));
            req.put("husbandName", mandatory("husbandName", request.getHusbandName()));
            req.put("husbandDateOfBirth", mandatory("husbandDateOfBirth", request.getHusbandDateOfBirth()));
            req.put("husbandPlaceOfBirth", mandatory("husbandPlaceOfBirth", request.getHusbandPlaceOfBirth()));
            req.put("address", mandatory("address", request.getAddress()));
            req.put("rtRw", mandatory("rtRw", request.getRtRw()));
            req.put("kelurahan", mandatory("kelurahan", request.getKelurahan()));
            req.put("kecamatan", mandatory("kecamatan", request.getKecamatan()));
            req.put("state", mandatory("state", request.getState()));
            req.put("country", mandatory("country", request.getCountry()));
            req.put("postalCode", mandatory("postalCode", request.getPostalCode()));
            req.put("savingAccountFlag", mandatory("savingAccountFlag", request.getSavingAccountFlag()));
            req.put("reasonOpenAccount", mandatory("reasonOpenAccount", request.getReasonOpenAccount()));
            req.put("fundSource", mandatory("fundSource", request.getFundSource()));
            req.put("houseCapacious", request.getHouseCapacious());
            req.put("houseCondition", request.getHouseCondition());
            req.put("houseRoofType", request.getHouseRoofType());
            req.put("houseWall", request.getHouseWall());
            req.put("houseFloor", request.getHouseFloor());
            req.put("houseElectricity", request.getHouseElectricity());
            req.put("houseSourceWater", request.getHouseSourceWater());
            req.put("houseIndexTotalScore", request.getHouseIndexTotalScore());
            req.put("totalAset", request.getTotalAset());
            req.put("loanAset", request.getLoanAset());
            req.put("netoAset", request.getNetoAset());
            req.put("formedByPersonnel", mandatory("formedByPersonnel", request.getFormedByPersonnel()));
            req.put("trained", request.getTrained());
            req.put("trainedDate", mandatory("trainedDate", request.getTrainedDate()));

            if (nullList.size() > 0) {
                response.setResponseCode("XX");
                response.setResponseMessage("Mandatory Field is Null : " + nullList.toString());
                return response;
            }

            for (Map.Entry<String, String> entry : req.entrySet())
                if (entry.getValue() == null)
                    LOGGER.trace("FIELD '" + entry.getKey() + "' IS NULL");

            Map<String, String> res = sendToESB("PRO_ADD_CUST_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            if (response.getResponseCode().equals("00")) {
                response.setCustomerId(res.get("customerId"));
                response.setCustomerNumber(res.get("customerNumber"));
            }
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/addLoan")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public LoanResponse addLoan(LoanRequest request) {

        LoanResponse response = new LoanResponse();
        if (ipIsValid()) {

            nullList = new ArrayList<String>();
            Map<String, String> req = processRequestMap();
            req.put("username", mandatory("username", request.getUsername()));
            req.put("imei", mandatory("imei", request.getImei()));
            req.put("transmissionDateAndTime", mandatory("transmissionDateAndTime", request.getTransmissionDateAndTime()));
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("custReffId", mandatory("custReffId", request.getCustReffId()));
            req.put("loanReffId", mandatory("loanReffId", request.getLoanReffId()));
            req.put("swId", mandatory("swId", request.getSwId()));
            req.put("perspective", request.getPerspective());
            req.put("prdOfferingShortName", mandatory("prdOfferingShortName", request.getPrdOfferingShortName()));
            req.put("tipeNasabah", mandatory("tipeNasabah", request.getTipeNasabah()));
            req.put("tipePembiayaan", mandatory("tipePembiayaan", request.getTipePembiayaan()));
            req.put("applicationDate", mandatory("applicationDate", request.getApplicationDate()));
            req.put("pembiayaanKe", request.getPembiayaanKe());
            req.put("penambahan", mandatory("penambahan", request.getPenambahan()));
            req.put("namaJenisUsaha", request.getNamaJenisUsaha());
            req.put("perlengkapanWarung", request.getPerlengkapanWarung());
            req.put("hargaPerlengkapanWarung", request.getHargaPerlengkapanWarung());
            req.put("ternak", request.getTernak());
            req.put("hargaTernak", request.getHargaTernak());
            req.put("bahanBaku", request.getBahanBaku());
            req.put("hargaBahanBaku", request.getHargaBahanBaku());
            req.put("pulsaHandphone", request.getPulsaHandphone());
            req.put("hargaPulsaHandphone", request.getHargaPulsaHandphone());
            req.put("other", request.getOther());
            req.put("hargaOther", request.getHargaOther());
            req.put("totalHarga", request.getTotalHarga());
            req.put("loanAmount", mandatory("loanAmount", request.getLoanAmount()));
            req.put("interestRate", mandatory("interestRate", request.getInterestRate()));
            req.put("noOfInstallments", mandatory("noOfInstallments", request.getNoOfInstallments()));
            req.put("disbursementDate", mandatory("disbursementDate", request.getDisbursementDate()));
            req.put("gracePeriodDuration", mandatory("gracePeriodDuration", request.getGracePeriodDuration()));
            req.put("fundId", request.getFundId());
            req.put("businessActivitiesId", request.getBusinessActivitiesId());
            req.put("externalId", request.getExternalId());
            req.put("otherInstitutionFlag", request.getOtherInstitutionFlag());
            req.put("institutionName", request.getInstitutionName());
            req.put("principalResidue", request.getPrincipalResidue());
            req.put("installmentResidue", request.getInstallmentResidue());
            req.put("installmentAmount", request.getInstallmentAmount());
            req.put("biCheckingResult", request.getBiCheckingResult());
            req.put("collectAbility", request.getCollectAbility());
            req.put("businessFlag", request.getBusinessFlag());
            req.put("businessType", request.getBusinessType());
            req.put("businessTypeValue", request.getBusinessTypeValue());
            req.put("placeStatus", request.getPlaceStatus());
            req.put("placeStatusValue", request.getPlaceStatusValue());
            req.put("age", mandatory("age", request.getAge()));
            req.put("income", mandatory("income", request.getIncome()));
            req.put("expense", mandatory("expense", request.getExpense()));
            req.put("netIncome", mandatory("netIncome", request.getNetIncome()));
            req.put("incomeFamily", mandatory("incomeFamily", request.getIncomeFamily()));
            req.put("businessFlagExp", mandatory("businessFlagExp", request.getBusinessFlagExp()));
            req.put("businessTypeExp", request.getBusinessTypeExp());
            req.put("businessTypeValueExp", request.getBusinessTypeValueExp());
            req.put("ageExp", mandatory("ageExp", request.getAgeExp()));
            req.put("businessTypePlan", request.getBusinessTypePlan());
            req.put("businessTypeValuePlan", request.getBusinessTypeValuePlan());
            req.put("placeStatusPlan", request.getPlaceStatusPlan());
            req.put("placeStatusValuePlan", request.getPlaceStatusValuePlan());
            req.put("hrb", request.getHrb());
            req.put("hrc", request.getHrc());
            req.put("deviation", request.getDeviation());
            req.put("flagId", request.getFlagId());
            req.put("comment", request.getComment());
            req.put("revenuePerMonth", mandatory("revenuePerMonth", request.getRevenuePerMonth()));
            req.put("customerNumber", mandatory("customerNumber", request.getCustomerNumber()));
            req.put("maxLoanAmount", request.getMaxLoanAmount());
            req.put("minLoanAmount", request.getMinLoanAmount());
            req.put("maxNoOfInstall", request.getMaxNoOfInstall());
            req.put("minNoOfInstall", request.getMinNoOfInstall());
            req.put("status", mandatory("status", request.getStatus()));
            req.put("interest", request.getInterest());
            req.put("holidayBonus", request.getHolidayBonus());
            req.put("orgDueDate", mandatory("orgDueDate", request.getOrgDueDate()));
            req.put("createdBy", mandatory("createdBy", request.getCreatedBy()));
            req.put("createdDate", mandatory("createdDate", request.getCreatedDate()));
            req.put("modifiedBy", mandatory("modifiedBy", request.getModifiedBy()));
            req.put("modifiedDate", mandatory("modifiedDate", request.getModifiedDate()));

            if (nullList.size() > 0) {
                response.setResponseCode("XX");
                response.setResponseMessage("Mandatory Field is Null : " + nullList.toString());
                return response;
            }

            for (Map.Entry<String, String> entry : req.entrySet())
                if (entry.getValue() == null)
                    LOGGER.trace("FIELD '" + entry.getKey() + "' IS NULL");

            Map<String, String> res = sendToESB("PRO_ADD_LOAN_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            if (response.getResponseCode().equals("00") || response.getResponseCode().equals("36")) {
                response.setProspectId(res.get("prospectId"));
                response.setNoappid(res.get("noappid"));
                response.setFirstInstallmentDate(res.get("firstInstallmentDate"));
                response.setLastInstallmentDate(res.get("lastInstallmentDate"));
            }
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/inputSurvey")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SurveyResponse inputSurvey(SurveyRequest request) {

        SurveyResponse response = new SurveyResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("customerId", request.getCustomerId());
            req.put("surveyId", request.getSurveyId());
            req.put("ppi", request.getPpi());
            int questionCount = 0;
            if (request.getQuestions() != null)
                questionCount = request.getQuestions().length;
            req.put("question.count", questionCount + "");
            for (int i = 0; i < questionCount; i++) {
                req.put("questionId-" + i, request.getQuestions()[i].getQuestionId());
                req.put("responseType-" + i, request.getQuestions()[i].getResponseType());
                req.put("responseValue-" + i, request.getQuestions()[i].getResponseValue());
            }
            req.put("reffId", request.getReffId());

            Map<String, String> res = sendToESB("PRO_INPUT_SURVEY_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/refresh/loan/amount")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public LoanRefreshResponse refreshLoan(LoanRefreshRequest request) {

        LoanRefreshResponse response = new LoanRefreshResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("username", request.getUsername());
            req.put("imei", request.getImei());
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());

            int count = 0;
            if (request.getLoanList() != null)
                count = request.getLoanList().length;
            req.put("loan.count", count + "");
            for (int i = 0; i < count; i++) {
                req.put("appId-" + i, request.getLoanList()[i].getAppId());
            }

            Map<String, String> res = sendToESB("PRO_LOAN_REFRESH_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            count = 0;
            if (res.get("loan.count") != null)
                count = Integer.valueOf(res.get("loan.count"));
            List<Loan> loanList = new ArrayList<Loan>();
            for (int i = 0; i < count; i++) {
                Loan loan = new Loan();
                loan.setAppId(res.get("appId-" + i));
                loan.setAmount(res.get("amount-" + i));
                loanList.add(loan);
            }
            response.setLoanList(loanList.toArray(new Loan[0]));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/submit/prs")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SubmitPrsResponse submitPRS(SubmitPrsRequest request) {

        SubmitPrsResponse response = new SubmitPrsResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("psCompanion", request.getPsCompanion());
            req.put("prsId", request.getPrsId());
            req.put("sentraId", request.getSentraId());
            req.put("prsDate", request.getPrsDate());
            req.put("tokenChallenge", request.getTokenChallenge());
            req.put("tokenResponse", request.getTokenResponse());

            int customerCount = 0;
            if (request.getCustomerList() != null)
                customerCount = request.getCustomerList().length;
            req.put("customer.count", customerCount + "");
            for (int i = 0; i < customerCount; i++) {
                Customer customer = request.getCustomerList()[i];
                req.put("customerId-" + i, customer.getCustomerId());
                req.put("isAttend-" + i, customer.getIsAttend());
                req.put("notAttendReason-" + i, customer.getNotAttendReason());
                int loanCount = 0;
                if (customer.getLoanList() != null)
                    loanCount = customer.getLoanList().length;
                req.put("loan.count-" + i, loanCount + "");
                for (int j = 0; j < loanCount; j++) {
                    LoanCustomer loan = customer.getLoanList()[j];
                    req.put("loanId-" + i + "-" + j, loan.getLoanId());
                    req.put("appId-" + i + "-" + j, loan.getAppid());
                    req.put("disbursementFlag-" + i + "-" + j, loan.getDisbursementFlag());
                    req.put("installmentPaymentAmount-" + i + "-" + j, loan.getInstallmentPaymentAmount());
                    req.put("useEmergencyFund-" + i + "-" + j, loan.getUseEmergencyFund());
                    req.put("isEarlyTermination-" + i + "-" + j, loan.getIsEarlyTermination());
                    req.put("earlyTerminationReason-" + i + "-" + j, loan.getEarlyTerminationReason());
                    req.put("marginDiscountDeviationFlag-" + i + "-" + j, loan.getMarginDiscountDeviationFlag());
                    req.put("marginDiscountDeviationAmount-" + i + "-" + j, loan.getMarginDiscountDeviationAmount());
                    req.put("marginDiscountDeviationPercentage-" + i + "-" + j, loan.getMarginDiscountDeviationPercentage());
                }
                int savingCount = 0;
                if (customer.getSavingList() != null)
                    savingCount = customer.getSavingList().length;
                req.put("saving.count-" + i, savingCount + "");
                for (int j = 0; j < savingCount; j++) {
                    Saving saving = customer.getSavingList()[j];
                    req.put("accountId-" + i + "-" + j, saving.getAccountId());
                    req.put("accountNumber-" + i + "-" + j, saving.getAccountNumber());
                    req.put("withdrawalAmount-" + i + "-" + j, saving.getWithdrawalAmount());
                    req.put("nextWithdrawalAmountPlan-" + i + "-" + j, saving.getNextWithdrawalAmountPlan());
                    req.put("isCloseSavingAccount-" + i + "-" + j, saving.getIsCloseSavingAccount());
                    req.put("clearBalance-" + i + "-" + j, saving.getClearBalance());
                    req.put("depositAmount-" + i + "-" + j, saving.getDepositAmount());
                }
            }

            Map<String, String> res = sendToESB("PRO_SUBMIT_PRS_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/inquiry/prs")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public InquiryPrsResponse inquiryPRS(InquiryPrsRequest request) {

        InquiryPrsResponse response = new InquiryPrsResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("prsId", request.getPrsId());

            Map<String, String> res = sendToESB("PRO_INQUIRY_PRS_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));

            List<CustomerInquiry> customerList = new ArrayList<CustomerInquiry>();
            int count = 0;
            if (res.get("customer.count") != null)
                count = Integer.valueOf(res.get("customer.count"));

            for (int i = 0; i < count; i++) {
                CustomerInquiry customerInquiry = new CustomerInquiry();
                customerInquiry.setCustomerId(res.get("customerId-" + i));
                customerInquiry.setStatus(res.get("status-" + i));
                customerInquiry.setTimestamp(res.get("timestamp-" + i));
                customerList.add(customerInquiry);
            }
            response.setCustomerList(customerList.toArray(new CustomerInquiry[0]));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/submit/nonprs")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SubmitNonPrsResponse submitNonPRS(SubmitNonPrsRequest request) {

        SubmitNonPrsResponse response = new SubmitNonPrsResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("tokenChallenge", request.getTokenChallenge());
            req.put("tokenResponse", request.getTokenResponse());

            int customerCount = 0;
            if (request.getCustomerList() != null)
                customerCount = request.getCustomerList().length;
            req.put("customer.count", customerCount + "");
            for (int i = 0; i < customerCount; i++) {
                CustomerNonPrs customer = request.getCustomerList()[i];
                req.put("customerId-" + i, customer.getCustomerId());
                int loanCount = 0;
                if (customer.getLoanList() != null)
                    loanCount = customer.getLoanList().length;
                req.put("loan.count-" + i, loanCount + "");
                for (int j = 0; j < loanCount; j++) {
                    LoanCustomerNonPrs loan = customer.getLoanList()[j];
                    req.put("appId-" + i + "-" + j, loan.getAppid());
                    req.put("installmentPaymentAmount-" + i + "-" + j, loan.getInstallmentPaymentAmount());
                }
                int savingCount = 0;
                if (customer.getSavingList() != null)
                    savingCount = customer.getSavingList().length;
                req.put("saving.count-" + i, savingCount + "");
                for (int j = 0; j < savingCount; j++) {
                    SavingNonPrs saving = customer.getSavingList()[j];
                    req.put("accountNumber-" + i + "-" + j, saving.getAccountNumber());
                    req.put("withdrawalAmount-" + i + "-" + j, saving.getWithdrawalAmount());
                    req.put("depositAmount-" + i + "-" + j, saving.getDepositAmount());
                }
            }

            Map<String, String> res = sendToESB("PRO_SUBMIT_NON_PRS_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/ps/assign")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public AssignPsResponse assignPS(AssignPsRequest request) {

        AssignPsResponse response = new AssignPsResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("approvedBy", request.getApprovedBy());

            int sentraCount = 0;
            if (request.getSentraList() != null)
                sentraCount = request.getSentraList().length;
            req.put("sentra.count", sentraCount + "");
            for (int i = 0; i < sentraCount; i++) {
                SentraPS sentra = request.getSentraList()[i];
                req.put("sentraId-" + i, sentra.getSentraId());
            }

            Map<String, String> res = sendToESB("PRO_ASSIGN_PS_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/claim/deviation")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DeviationResponse claimDeviation(DeviationRequest request) {

        DeviationResponse response = new DeviationResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("noappid", request.getNoappid());
            req.put("userApproval", request.getUserApproval());
            req.put("role", request.getRole());

            Map<String, String> res = sendToESB("PRO_DEVIATION_CLAIM_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/claim/insurance")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public InsuranceResponse claimInsurance(InsuranceRequest request) {

        InsuranceResponse response = new InsuranceResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("customerNumber", request.getCustomerNumber());
            req.put("spouseFlag", request.getSpouseFlag());
            req.put("customerFlag", request.getCustomerFlag());
            req.put("oldStatus", request.getOldStatus());
            req.put("newStatus", request.getNewStatus());
            req.put("note", request.getDeathCause());
            req.put("createdBy", request.getCreatedBy());
            req.put("createdDate", request.getCreatedDate());
            req.put("approvedBy", request.getApprovedBy());
            req.put("approvedDate", request.getApprovedDate());

            Map<String, String> res = sendToESB("PRO_INSURANCE_CLAIM_MPRO", req);
            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
        } else
            response.setResponseCode("12");
        return response;
    }

    @POST
    @Path("/trx/inquiry")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public TrxInquiryResponse trxInquiry(TrxInquiryRequest request) {

        TrxInquiryResponse response = new TrxInquiryResponse();
        if (ipIsValid()) {

            Map<String, String> req = processRequestMap();
            req.put("transmissionDateAndTime", request.getTransmissionDateAndTime());
            req.put("retrievalReferenceNumber", request.getRetrievalReferenceNumber());
            req.put("imei", request.getImei());
            req.put("username", request.getUsername());
            req.put("accountId", request.getAccountId());
            req.put("accountType", request.getAccountType());

            Map<String, String> res = sendToESB("PRO_TRX_INQ_MPRO", req);
            int count = 0;
            if (res.get("trx.count") != null && !res.get("trx.count").equals(""))
                count = Integer.valueOf(res.get("trx.count"));

            response.setResponseCode(res.get("responseCode"));
            response.setResponseMessage(res.get("responseMessage"));
            response.setStatus(res.get("status"));
            response.setSisaMarjin(res.get("sisaMarjin"));
            response.setSisaPokok(res.get("sisaPokok"));
            response.setNominalAngsuran(res.get("nominalAngsuran"));
            response.setLastPaymentDate(res.get("lastPaymentDate"));
            response.setTotalDanaTalangan(res.get("totalDanaTalangan"));
            response.setTanggal(res.get("tanggal"));
            response.setNarasi(res.get("narasi"));
            response.setAmount(res.get("amount"));

            if (count == 0) {
                response.setTrxHistories(new ArrayList<TrxHistory>(0));
            } else {
                List<TrxHistory> trxHistoryList = new ArrayList<TrxHistory>();
                for (int i = 0; i < count; i++) {
                    TrxHistory trxHistory = new TrxHistory();
                    trxHistory.setAmount(res.get("amount-" + i));
                    trxHistory.setNarasi(res.get("narasi-" + i));
                    trxHistory.setTanggal(res.get("tanggal-" + i));
                    trxHistoryList.add(trxHistory);
                }
                response.setTrxHistories(trxHistoryList);
            }
        } else
            response.setResponseCode("12");
        return response;
    }

    protected String ifNullPutBlank(String input) {
        if (input == null)
            return "";
        return input;
    }

    public void setMessageHolder(MessageHolder messageHolder) {
        this.messageHolder = messageHolder;
    }

    public void setCommunicator(MyTransport communicator) {
        this.communicator = communicator;
    }

    public void setlistAllowedIp(String listAllowedIp) {
        this.listAllowedIp = listAllowedIp;
    }

    private Map<String, String> sendToESB(String mappingCode, Map<String, String> req) {
        MyMessage wsServer = communicator.sendMessage(mappingCode, req);
        return wsServer.getValueHolder();
    }

    private Map<String, String> processRequestMap() {
        Message message = PhaseInterceptorChain.getCurrentMessage();
        HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
        LOGGER.info("Receive Webservice REST Message Executing operation execute from address "
                + request.getRemoteAddr() + " and socket " + request.getRemotePort());
        Map<String, String> req = new ConcurrentHashMap<String, String>();
        // masukin raw message
        String cxfId = (String) message.get("org.apache.cxf.interceptor.LoggingMessage.ID");
        String rawMessage = (String) messageHolder.getMessageList().remove(cxfId);
        req.put("rawMessage", rawMessage);
        req.put("cxfId", cxfId);
        req.put("remoteAddress", request.getRemoteAddr());
        req.put("listAllowedIp", listAllowedIp);

        return req;
    }

    public String mandatory(String key, String data) {
        if (data == null) {
            nullList.add(key);
            return "";
        }
        return data;
    }
}
