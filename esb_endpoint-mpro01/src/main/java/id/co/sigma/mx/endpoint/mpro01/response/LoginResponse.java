package id.co.sigma.mx.endpoint.mpro01.response;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "loginResponse")
public class LoginResponse extends BaseResponse {
	private String officeId;
	private String state;
	private String[] role;
	private String[] centers;

	public String getOfficeId() {
		return officeId;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String[] getRole() {
		return role;
	}

	public void setRole(String[] role) {
		this.role = role;
	}

	public String[] getCenters() {
		return centers;
	}

	public void setCenters(String[] centers) {
		this.centers = centers;
	}

	@Override
	public String toString() {
		return "LoginResponse [officeId=" + officeId + ", state=" + state + ", role=" + Arrays.toString(role)
				+ ", centers=" + Arrays.toString(centers) + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
