package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "resetPasswordResponse")
public class PasswordResetResponse extends BaseResponse {

	@Override
	public String toString() {
		return "ResetPasswordResponse [getResponseCode()=" + getResponseCode() + ", getResponseMessage()="
				+ getResponseMessage() + "]";
	}

}
