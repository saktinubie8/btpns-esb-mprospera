package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "deviationRequest")
public class DeviationRequest extends BaseRequest {

	private String imei;
	private String username;
	private String noappid;
	private String userApproval;
	private String role;

	public String getNoappid() {
		return noappid;
	}

	public void setNoappid(String noappid) {
		this.noappid = noappid;
	}

	public String getUserApproval() {
		return userApproval;
	}

	public void setUserApproval(String userApproval) {
		this.userApproval = userApproval;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String uername) {
		this.username = uername;
	}

	@Override
	public String toString() {
		return "DeviationRequest [imei=" + imei + ", username=" + username + ", noappid=" + noappid + ", userApproval="
				+ userApproval + ", role=" + role + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}

}
