package id.co.sigma.mx.endpoint.mpro01.request;

import java.util.Arrays;

public class SentraPS {

	private String sentraId;

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	@Override
	public String toString() {
		return "SentraPS [sentraId=" + sentraId + "]";
	}

}
