package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "surveyRequest")
public class SurveyRequest extends BaseRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private String customerId;
	private String surveyId;
	private Questions[] questions;
	private String reffId;
	private String ppi;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public Questions[] getQuestions() {
		return questions;
	}

	public void setQuestions(Questions[] questions) {
		this.questions = questions;
	}

	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	public String getPpi() {
		return ppi;
	}

	public void setPpi(String ppi) {
		this.ppi = ppi;
	}

	@Override
	public String toString() {
		return "SurveyRequest [username=" + username + ", imei=" + imei + ", customerId=" + customerId + ", surveyId="
				+ surveyId + ", questions=" + Arrays.toString(questions) + ", reffId=" + reffId + ", isPpi=" + ppi
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}

}
