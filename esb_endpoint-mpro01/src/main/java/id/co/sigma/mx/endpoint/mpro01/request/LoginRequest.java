package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "loginRequest")
public class LoginRequest extends BaseRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String imei;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	@Override
	public String toString() {
		return "LoginRequest [username=" + username + ", password=" + password + ", imei=" + imei + "]";
	}
}
