package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sentraRequest")
public class SentraRequest extends BaseRequest {

	// untuk add
	private String username;
	private String imei;
	private String areaId;
	private String sentraName;
	private String sentraOwnerName;
	private String startedDate;
	private String sentraAddress;
	private String rtRw;
	private String sentraLeader;
	private String prsDay;
	private String sentraPicture;
	private String longitude;
	private String latitude;
	private String action;
	private String sentraId;

	// Prospera purpose
	private String kecamatan;
	private String kelurahan;
	private String officeCode;
	private String meetingPlace;
	private String dayNumber;
	private String recurAfter;
	private String postcode;
	private String reffId;
	private String provinsi;
	private String kabupaten;
	private String customerNumberLeader;
	private String actionTime;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getSentraName() {
		return sentraName;
	}

	public void setSentraName(String sentraName) {
		this.sentraName = sentraName;
	}

	public String getSentraOwnerName() {
		return sentraOwnerName;
	}

	public void setSentraOwnerName(String sentraOwnerName) {
		this.sentraOwnerName = sentraOwnerName;
	}

	public String getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}

	public String getSentraAddress() {
		return sentraAddress;
	}

	public void setSentraAddress(String sentraAddress) {
		this.sentraAddress = sentraAddress;
	}

	public String getRtRw() {
		if(rtRw==null)
			return "";
		return rtRw;
	}

	public void setRtRw(String rtRw) {
		this.rtRw = rtRw;
	}

	public String getSentraLeader() {
		return sentraLeader;
	}

	public void setSentraLeader(String sentraLeader) {
		this.sentraLeader = sentraLeader;
	}

	public String getPrsDay() {
		return prsDay;
	}

	public void setPrsDay(String prsDay) {
		this.prsDay = prsDay;
	}

	public String getSentraPicture() {
		return sentraPicture;
	}

	public void setSentraPicture(String sentraPicture) {
		this.sentraPicture = sentraPicture;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getMeetingPlace() {
		return meetingPlace;
	}

	public void setMeetingPlace(String meetingPlace) {
		this.meetingPlace = meetingPlace;
	}

	public String getDayNumber() {
		return dayNumber;
	}

	public void setDayNumber(String dayNumber) {
		this.dayNumber = dayNumber;
	}

	public String getRecurAfter() {
		return recurAfter;
	}

	public void setRecurAfter(String recurAfter) {
		this.recurAfter = recurAfter;
	}

	public String getPostcode() {
		if(postcode==null)
			return "";
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	public String getCustomerNumberLeader() {
		if(customerNumberLeader==null)
			return "";
		return customerNumberLeader;
	}

	public void setCustomerNumberLeader(String customerNumberLeader) {
		this.customerNumberLeader = customerNumberLeader;
	}

	public String getProvinsi() {
		if(provinsi==null)
			return "";
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getKabupaten() {
		if(kabupaten==null)
			return "";
		return kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getActionTime() {
		return actionTime;
	}

	public void setActionTime(String actionTime) {
		this.actionTime = actionTime;
	}

	@Override
	public String toString() {
		return "SentraRequest [username=" + username + ", imei=" + imei + ", areaId=" + areaId + ", sentraName="
				+ sentraName + ", sentraOwnerName=" + sentraOwnerName + ", startedDate=" + startedDate
				+ ", sentraAddress=" + sentraAddress + ", rtRw=" + rtRw + ", sentraLeader=" + sentraLeader + ", prsDay="
				+ prsDay + ", sentraPicture=" + sentraPicture + ", longitude=" + longitude + ", latitude=" + latitude
				+ ", action=" + action + ", sentraId=" + sentraId + ", kecamatan=" + kecamatan + ", kelurahan="
				+ kelurahan + ", officeCode=" + officeCode + ", meetingPlace=" + meetingPlace + ", dayNumber="
				+ dayNumber + ", recurAfter=" + recurAfter + ", postcode=" + postcode + ", reffId=" + reffId
				+ ", provinsi=" + provinsi + ", kabupaten=" + kabupaten + ", customerNumberLeader="
				+ customerNumberLeader + ", actionTime=" + actionTime + ", getTransmissionDateAndTime()="
				+ getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
				+ "]";
	}

}
