package id.co.sigma.mx.endpoint.mpro01.response;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoanRefreshResponse extends BaseResponse {

	private Loan[] loanList;

	public Loan[] getLoanList() {
		return loanList;
	}

	public void setLoanList(Loan[] loanList) {
		this.loanList = loanList;
	}

	@Override
	public String toString() {
		return "RefreshLoanResponse [loanList=" + Arrays.toString(loanList) + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
