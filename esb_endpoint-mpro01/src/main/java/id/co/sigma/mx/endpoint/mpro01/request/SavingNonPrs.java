package id.co.sigma.mx.endpoint.mpro01.request;

public class SavingNonPrs {

	private String accountNumber;
	private String withdrawalAmount;
	private String depositAmount;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getWithdrawalAmount() {
		if(withdrawalAmount==null)
			return "";
		return withdrawalAmount;
	}

	public void setWithdrawalAmount(String withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	public String getDepositAmount() {
		if(depositAmount==null)
			return "";
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	@Override
	public String toString() {
		return "SavingNonPrs [accountNumber=" + accountNumber + ", withdrawalAmount=" + withdrawalAmount
				+ ", depositAmount=" + depositAmount + "]";
	}

}
