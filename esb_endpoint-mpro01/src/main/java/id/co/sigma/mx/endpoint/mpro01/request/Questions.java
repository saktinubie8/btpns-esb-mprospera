package id.co.sigma.mx.endpoint.mpro01.request;

public class Questions {
	private String questionId;
	private String responseType;
	private String responseValue;

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getResponseValue() {
		return responseValue;
	}

	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}

	@Override
	public String toString() {
		return "Questions [questionId=" + questionId + ", responseType=" + responseType + ", responseValue="
				+ responseValue + "]";
	}

}
