package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "echoResponse")
public class EchoResponse  extends BaseResponse {
}
