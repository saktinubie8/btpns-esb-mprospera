package id.co.sigma.mx.endpoint.mpro01.response;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InquiryPrsResponse extends BaseResponse {

	private CustomerInquiry[] customerList;

	public CustomerInquiry[] getCustomerList() {
		return customerList;
	}

	public void setCustomerList(CustomerInquiry[] customerList) {
		this.customerList = customerList;
	}

	@Override
	public String toString() {
		return "InquiryPrsResponse [customerList=" + Arrays.toString(customerList) + ", getResponseCode()="
				+ getResponseCode() + ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
