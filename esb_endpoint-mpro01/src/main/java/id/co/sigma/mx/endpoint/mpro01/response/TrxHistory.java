package id.co.sigma.mx.endpoint.mpro01.response;

public class TrxHistory {

	private String tanggal;
	private String narasi;
	private String amount;

	public String getTanggal() {
		return tanggal;
	}

	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}

	public String getNarasi() {
		return narasi;
	}

	public void setNarasi(String narasi) {
		this.narasi = narasi;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TrxHistory [tanggal=" + tanggal + ", narasi=" + narasi + ", amount=" + amount + "]";
	}

}
