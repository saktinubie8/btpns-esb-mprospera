package id.co.sigma.mx.endpoint.mpro01.request;

public class Loan {

	private String appId;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Override
	public String toString() {
		return "Loan [appId=" + appId + "]";
	}
	
}
