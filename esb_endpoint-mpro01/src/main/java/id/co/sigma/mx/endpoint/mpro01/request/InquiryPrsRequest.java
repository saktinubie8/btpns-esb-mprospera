package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InquiryPrsRequest extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private String prsId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getPrsId() {
		return prsId;
	}

	public void setPrsId(String prsId) {
		this.prsId = prsId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InquiryPrsRequest [username=" + username + ", imei=" + imei + ", prsId=" + prsId
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}

}
