package id.co.sigma.mx.endpoint.mpro01;

import java.util.HashMap;
import java.util.Map;

public class MessageHolder {
	private Map<String,Object> messageList = new HashMap<String,Object>();

	public Map<String, Object> getMessageList() {
		return messageList;
	}

	public void setMessageList(Map<String, Object> messageList) {
		this.messageList = messageList;
	}
}
