package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "surveyResponse")
public class SurveyResponse extends BaseResponse {
}
