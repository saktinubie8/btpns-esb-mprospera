package id.co.sigma.mx.endpoint.mpro01.response;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubmitNonPrsResponse extends BaseResponse {

	@Override
	public String toString() {
		return "SubmitPrsResponse [getResponseCode()=" + getResponseCode() + ", getResponseMessage()="
				+ getResponseMessage() + "]";
	}

}
