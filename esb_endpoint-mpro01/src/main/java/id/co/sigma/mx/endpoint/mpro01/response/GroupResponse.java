package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "groupResponse")
public class GroupResponse extends BaseResponse{
	
	private String groupId;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	

}
