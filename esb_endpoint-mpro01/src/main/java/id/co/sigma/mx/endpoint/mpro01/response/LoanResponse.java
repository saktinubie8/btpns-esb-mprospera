package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "loanResponse")
public class LoanResponse extends BaseResponse {

	private String prospectId;
	private String noappid;
	private String firstInstallmentDate;
	private String lastInstallmentDate;

	public String getProspectId() {
		return prospectId;
	}

	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public String getNoappid() {
		return noappid;
	}

	public void setNoappid(String noappid) {
		this.noappid = noappid;
	}

	public String getFirstInstallmentDate() {
		return firstInstallmentDate;
	}

	public void setFirstInstallmentDate(String firstInstallmentDate) {
		this.firstInstallmentDate = firstInstallmentDate;
	}

	public String getLastInstallmentDate() {
		return lastInstallmentDate;
	}

	public void setLastInstallmentDate(String lastInstallmentDate) {
		this.lastInstallmentDate = lastInstallmentDate;
	}

	@Override
	public String toString() {
		return "LoanResponse [prospectId=" + prospectId + ", noappid=" + noappid + ", firstInstallmentDate="
				+ firstInstallmentDate + ", lastInstallmentDate=" + lastInstallmentDate + ", getResponseCode()="
				+ getResponseCode() + ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
