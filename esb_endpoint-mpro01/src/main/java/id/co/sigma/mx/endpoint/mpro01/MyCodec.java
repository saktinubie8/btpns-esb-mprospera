package id.co.sigma.mx.endpoint.mpro01;

import id.co.sigma.mx.message.ExternalMessageFactory;
import id.co.sigma.mx.message.SimpleExternalMessageFactory;
import id.co.sigma.mx.message.common.CommonCodec;
import id.co.sigma.mx.util.PropertiesUtil;

import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyCodec extends CommonCodec<MyMessage> {
	private final ExternalMessageFactory<MyMessage> externalMessageFactory;
	private static final transient Logger LOGGER = LoggerFactory.getLogger(MyCodec.class);

	public MyCodec() {
		externalMessageFactory = new SimpleExternalMessageFactory<MyMessage>(MyMessage.class);
	}

	@Override
	public String encode(MyMessage unpackedMessage) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Encoding packed message:"+unpackedMessage);
		}
		Properties p = new Properties();

		for(String key : unpackedMessage.getValueHolder().keySet())
		{
			p.put(key, unpackedMessage.getValue(key));
		}
		return PropertiesUtil.toString(p);
	}

	@Override
	public MyMessage decode(String packedMessage) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Decoding packed message:"+packedMessage);
		}
		MyMessage m = new MyMessage();

		for (Entry<Object, Object> e : PropertiesUtil.fromString(packedMessage).entrySet()) {
			m.setValue(String.valueOf(e.getKey()), String.valueOf(e.getValue()));
		}
		return m;
	}

	@Override
	protected String extractPackagerCode(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String readPackagerCode(MyMessage message) {
		return null;
	}
}