package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sentraResponse")
public class SentraResponse extends BaseResponse {

	private String sentraId;
	private String kodeSentra;

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getKodeSentra() {
		return kodeSentra;
	}

	public void setKodeSentra(String kodeSentra) {
		this.kodeSentra = kodeSentra;
	}

}
