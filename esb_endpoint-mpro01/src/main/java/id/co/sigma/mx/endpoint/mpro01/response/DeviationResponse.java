package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "deviationResponse")
public class DeviationResponse extends BaseResponse {

	@Override
	public String toString() {
		return "DeviationResponse [getResponseCode()=" + getResponseCode() + ", getResponseMessage()="
				+ getResponseMessage() + "]";
	}

}
