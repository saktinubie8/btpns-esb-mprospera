package id.co.sigma.mx.endpoint.mpro01.request;

public class LoanCustomerNonPrs {

	private String appid;
	private String installmentPaymentAmount;

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getInstallmentPaymentAmount() {
		if(installmentPaymentAmount==null)
			return "";
		return installmentPaymentAmount;
	}

	public void setInstallmentPaymentAmount(String installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}

	@Override
	public String toString() {
		return "LoanCustomerNonPrs [appid=" + appid + ", installmentPaymentAmount=" + installmentPaymentAmount + "]";
	}

}
