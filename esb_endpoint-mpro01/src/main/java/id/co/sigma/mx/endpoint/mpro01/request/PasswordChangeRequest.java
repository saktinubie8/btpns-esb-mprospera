package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "changePasswordRequest")
public class PasswordChangeRequest extends BaseRequest {

    private static final long serialVersionUID = 1L;
    private String transmissionDateAndTime;
    private String retrievalReferenceNumber;
    private String username;
    private String imei;
    private String oldPassword;
    private String newPassword;

    public String getTransmissionDateAndTime() {
        return transmissionDateAndTime;
    }

    public void setTransmissionDateAndTime(String transmissionDateAndTime) {
        this.transmissionDateAndTime = transmissionDateAndTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}