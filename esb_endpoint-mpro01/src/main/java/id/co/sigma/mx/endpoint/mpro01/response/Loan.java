package id.co.sigma.mx.endpoint.mpro01.response;

public class Loan {

	private String appId;
	private String amount;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Loan [appId=" + appId + ", amount=" + amount + "]";
	}

}
