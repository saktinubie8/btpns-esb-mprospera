package id.co.sigma.mx.endpoint.mpro01.response;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "trxInquiryResponse")
public class TrxInquiryResponse extends BaseResponse {

	private String status;
	private String sisaMarjin;
	private String sisaPokok;
	private String nominalAngsuran;
	private String lastPaymentDate;
	private String totalDanaTalangan;
	private List<TrxHistory> trxHistories;
	private String tanggal;
	private String narasi;
	private String amount;

	public List<TrxHistory> getTrxHistories() {
		return trxHistories;
	}

	public void setTrxHistories(List<TrxHistory> trxHistories) {
		this.trxHistories = trxHistories;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSisaMarjin() {
		return sisaMarjin;
	}

	public void setSisaMarjin(String sisaMarjin) {
		this.sisaMarjin = sisaMarjin;
	}

	public String getSisaPokok() {
		return sisaPokok;
	}

	public void setSisaPokok(String sisaPokok) {
		this.sisaPokok = sisaPokok;
	}

	public String getNominalAngsuran() {
		return nominalAngsuran;
	}

	public void setNominalAngsuran(String nominalAngsuran) {
		this.nominalAngsuran = nominalAngsuran;
	}

	public String getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public String getTotalDanaTalangan() {
		return totalDanaTalangan;
	}

	public void setTotalDanaTalangan(String totalDanaTalangan) {
		this.totalDanaTalangan = totalDanaTalangan;
	}

	public String getTanggal() {
		return tanggal;
	}

	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}

	public String getNarasi() {
		return narasi;
	}

	public void setNarasi(String narasi) {
		this.narasi = narasi;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TrxInquiryResponse [status=" + status + ", sisaMarjin=" + sisaMarjin + ", sisaPokok=" + sisaPokok
				+ ", nominalAngsuran=" + nominalAngsuran + ", lastPaymentDate=" + lastPaymentDate
				+ ", totalDanaTalangan=" + totalDanaTalangan + ", trxHistories=" + trxHistories + ", tanggal=" + tanggal
				+ ", narasi=" + narasi + ", amount=" + amount + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
