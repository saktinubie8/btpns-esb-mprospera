package id.co.sigma.mx.endpoint.mpro01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import id.co.sigma.mx.message.InternalMessage;
import id.co.sigma.mx.message.ExternalMessage;
import id.co.sigma.mx.util.StringHelper;

public class CommonPropertySetter {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonPropertySetter.class);
	public InternalMessage apply(InternalMessage iMsg,ExternalMessage eMsg) {

		// Set log storage key
		String transmissionDateTime = eMsg.getValue("transmissionDateAndTime");
		String stan = StringHelper.lpad(eMsg.getValue("systemTraceNumber"),6,'0');
		String channelRefNum = StringHelper.lpad(eMsg.getValue("retrievalReferenceNumber"),12,'0');
		String logStorageKey = transmissionDateTime + "$" + channelRefNum + "$" + stan;	
		iMsg.setValue("/data/logStorageKey/text()", logStorageKey);

		return iMsg;
	}
}