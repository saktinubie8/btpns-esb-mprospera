package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "insuranceResponse")
public class InsuranceResponse extends BaseResponse {

	@Override
	public String toString() {
		return "InsuranceResponse [getResponseCode()=" + getResponseCode() + ", getResponseMessage()="
				+ getResponseMessage() + "]";
	}

}
