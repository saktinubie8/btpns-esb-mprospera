package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "groupRequest")
public class GroupRequest extends BaseRequest {

	// untuk add
	private String username;
	private String imei;
	private String groupName;
	private String groupLeader;
	private String longitude;
	private String latitude;
	private String action;
	private String groupId;
	private String sentraId;

	// For prospera purpose
	private String trainedDate;
	private String customerNumberCenter;
	private String reffId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupLeader() {
		if(groupLeader==null)
			return "";
		return groupLeader;
	}

	public void setGroupLeader(String groupLeader) {
		this.groupLeader = groupLeader;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getCustomerNumberCenter() {
		return customerNumberCenter;
	}

	public void setCustomerNumberCenter(String customerNumberCenter) {
		this.customerNumberCenter = customerNumberCenter;
	}

	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	public String getTrainedDate() {
		return trainedDate;
	}

	public void setTrainedDate(String trainedDate) {
		this.trainedDate = trainedDate;
	}

	@Override
	public String toString() {
		return "GroupRequest [username=" + username + ", imei=" + imei + ", groupName=" + groupName + ", groupLeader="
				+ groupLeader + ", longitude=" + longitude + ", latitude=" + latitude + ", action=" + action
				+ ", groupId=" + groupId + ", sentraId=" + sentraId + ", trainedDate=" + trainedDate
				+ ", customerNumberCenter=" + customerNumberCenter + ", reffId=" + reffId
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}
}
