package id.co.sigma.mx.endpoint.mpro01;

import id.co.sigma.mx.message.BaseMessageTranslator;
import id.co.sigma.mx.message.InternalMessage;
import id.co.sigma.mx.stan.StanGenerator;
import id.co.sigma.mx.message.SimpleExternalMessageFactory;
import javax.annotation.PostConstruct;
import static id.co.sigma.mx.message.InternalMessage.FIELD_TX_ID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * Translate to external message
 *
 * @author <a href="mailto:ichsan@gmail.com">Muhammad Ichsan<a/>
 *
 */
public class MyTranslator extends BaseMessageTranslator<MyMessage> {
	private static final transient Logger LOGGER = LoggerFactory.getLogger(MyTranslator.class);
	private StanGenerator stanGenerator;

	public MyTranslator() {
		setExternalMessageFactory(SimpleExternalMessageFactory.newInstance(MyMessage.class));
	}

	/**
	 * Translate request message from channel into MX
	 */
	@Override
	public InternalMessage from(MyMessage from, String endpoint) {
		InternalMessage imsg = super.from(from, endpoint);
		if (from.getAssocKey() == null) {
			String stan = "" + this.stanGenerator.generateStan(endpoint);
			imsg.setValue("/custom/" + endpoint + "ChannelStan/text()", stan);
			from.setAssocKey(stan);
		} else {
			imsg.setValue("/custom/" + endpoint + "ChannelStan/text()", from.getAssocKey());
		}
		if (imsg.getValue(FIELD_TX_ID) == null) {
			imsg.setValue(FIELD_TX_ID, from.getTransactionId());
			if (LOGGER.isDebugEnabled())
				LOGGER.debug("MX Transaction ID = " + from.getTransactionId());
		}
		return imsg;
	}

	/**
	 * Translate MX message into response message for channel
	 */
	@Override
	public MyMessage to(InternalMessage from, String endpoint) {
		// Pure translation
		MyMessage xMsg = super.to(from, endpoint);

		if (xMsg.isRequest()) {
			xMsg.setAssocKey(xMsg.getValue("assocKey"));
			if (xMsg.getAssocKey() == null) {
				String stan = "" + this.stanGenerator.generateStan(endpoint);
				LOGGER.trace("Setup stan for this message [ " + stan + " ]");
				xMsg.setAssocKey(stan);
			}
		} else
			xMsg.setAssocKey(from.getValue("/custom/" + endpoint + "ChannelStan/text()"));

		return xMsg;
	}

	@Override
	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(stanGenerator, "Properties stanGenerator is required!");

		super.afterPropertiesSet();
	}

	/* Accessors ********************************* */

	public void setStanGenerator(StanGenerator stanGenerator) {
		this.stanGenerator = stanGenerator;
	}
}
