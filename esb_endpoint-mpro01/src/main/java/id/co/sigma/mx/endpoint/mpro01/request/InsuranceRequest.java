package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "insuranceRequest")
public class InsuranceRequest extends BaseRequest {

	private String imei;
	private String username;
	private String customerNumber;
	private String spouseFlag;
	private String customerFlag;
	private String oldStatus;
	private String newStatus;
	private String deathCause;
	private String createdBy;
	private String createdDate;
	private String approvedBy;
	private String approvedDate;

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getSpouseFlag() {
		return spouseFlag;
	}

	public void setSpouseFlag(String spouseFlag) {
		this.spouseFlag = spouseFlag;
	}

	public String getCustomerFlag() {
		return customerFlag;
	}

	public void setCustomerFlag(String customerFlag) {
		this.customerFlag = customerFlag;
	}

	public String getOldStatus() {
		if(oldStatus==null)
			return "";
		return oldStatus;
	}

	public void setOldStatus(String oldStatus) {
		this.oldStatus = oldStatus;
	}

	public String getNewStatus() {
		if(newStatus==null)
			return "";
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getDeathCause() {
		return deathCause;
	}

	public void setDeathCause(String deathCause) {
		this.deathCause = deathCause;
	}

	@Override
	public String toString() {
		return "InsuranceRequest [imei=" + imei + ", username=" + username + ", customerNumber=" + customerNumber
				+ ", spouseFlag=" + spouseFlag + ", customerFlag=" + customerFlag + ", oldStatus=" + oldStatus
				+ ", newStatus=" + newStatus + ", deathCause=" + deathCause + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", approvedBy=" + approvedBy + ", approvedDate=" + approvedDate
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}

}
