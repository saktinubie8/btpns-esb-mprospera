package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "trxInquiryRequest")
public class TrxInquiryRequest extends BaseRequest {

	private String username;
	private String imei;
	private String accountId;
	private String accountType;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	@Override
	public String toString() {
		return "TrxInquiryRequest [username=" + username + ", imei=" + imei + ", accountId=" + accountId
				+ ", accountType=" + accountType + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}

}
