package id.co.sigma.mx.endpoint.mpro01.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "customerResponse")
public class CustomerResponse extends BaseResponse {

	private String customerId;
	private String customerNumber;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	@Override
	public String toString() {
		return "CustomerResponse [customerId=" + customerId + ", customerNumber=" + customerNumber
				+ ", getResponseCode()=" + getResponseCode() + ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
