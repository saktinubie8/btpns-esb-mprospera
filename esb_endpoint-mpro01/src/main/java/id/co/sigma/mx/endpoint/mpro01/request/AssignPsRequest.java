package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssignPsRequest extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private SentraPS[] sentraList;
	private String approvedBy;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public SentraPS[] getSentraList() {
		return sentraList;
	}

	public void setSentraList(SentraPS[] sentraList) {
		this.sentraList = sentraList;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	@Override
	public String toString() {
		return "AssignPsRequest [username=" + username + ", imei=" + imei + ", sentraList="
				+ Arrays.toString(sentraList) + ", approvedBy=" + approvedBy + ", getTransmissionDateAndTime()="
				+ getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
				+ "]";
	}

}
