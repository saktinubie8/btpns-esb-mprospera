package id.co.sigma.mx.endpoint.mpro01;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageUtils;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingIn extends LoggingInInterceptor {

	private static final transient Logger LOGGER = LoggerFactory
			.getLogger(LoggingIn.class);
	
	public void setMessageHolder(MessageHolder messageHolder) {
		this.messageHolder = messageHolder;
	}

	private MessageHolder messageHolder;
	
	public LoggingIn()
	{
		super("receive");
		this.setPrettyLogging(true);
	}

	@Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {
    	Message message = PhaseInterceptorChain.getCurrentMessage();
    	String id = (String)message.get("org.apache.cxf.interceptor.LoggingMessage.ID");
    	messageHolder.getMessageList().put(""+id, loggingMessage.toString());
    	
    	boolean requestor = MessageUtils.isRequestor(message);
    	boolean outbound = MessageUtils.isOutbound(message);
    	if (requestor) {
    	    if (!outbound) {
    	    	//This is your RESPONSE message from WS Server (we receive)
    	    }
    	    else
    	    {
    	    	//This is your REQUEST message to WS Server (we send)
    	    }
    	} else {
    	    if (!outbound) {
    	    	//This is your REQUEST message from client (we receive)
    	        
    	    }
    	    else
    	    {
    	    	//This is your RESPONSE message to client (we send)
    	    }
    	}
    	
        return super.formatLoggingMessage(loggingMessage);
    }
}
