package id.co.sigma.mx.endpoint.mpro01.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "resetPasswordRequest")
public class PasswordResetRequest extends BaseRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private String oldPassword;
	private String newPassword;
	private String tokenChallenge;
	private String tokenResponse;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getTokenResponse() {
		return tokenResponse;
	}

	public void setTokenResponse(String tokenResponse) {
		this.tokenResponse = tokenResponse;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getTokenChallenge() {
		return tokenChallenge;
	}

	public void setTokenChallenge(String tokenChallenge) {
		this.tokenChallenge = tokenChallenge;
	}

	@Override
	public String toString() {
		return "ResetPasswordRequest [username=" + username + ", imei=" + imei + ", oldPassword=" + oldPassword
				+ ", newPassword=" + newPassword + ", tokenChallenge=" + tokenChallenge + ", tokenResponse="
				+ tokenResponse + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}

}
