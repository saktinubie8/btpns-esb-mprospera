package id.co.sigma.mx.endpoint.mpro01.request;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "customerRequest")
public class CustomerRequest extends BaseRequest {

	private String username;
	private String imei;
	private String customerNumberCenter;
	private String customerNumberGroup;
	private String actionFlag;
	private String reffId;
	private String createdBy;
	private String createdDate;
	private String groupFlag;
	private String fullName;
	private String firstName;
	private String nickName;
	private String governmentId;
	private String dateOfGovernmentId;
	private String dateOfBirth;
	private String placeOfBirth;
	private String telephone;
	private String motherName;
	private String gender;
	private String maritalStatus;
	private String occupation;
	private String numberOfChildren;
	private String businessIncomeFamily;
	private String citizenShip;
	private String religion;
	private String ethinicity;
	private String citizenType;
	private String residenceStatus;
	private String latestEducation;
	private String husbandName;
	private String husbandDateOfBirth;
	private String husbandPlaceOfBirth;
	private String address;
	private String rtRw;
	private String kelurahan;
	private String kecamatan;
	private String state;
	private String country;
	private String postalCode;
	private String savingAccountFlag;
	private String reasonOpenAccount;
	private String fundSource;
	private String houseCapacious;
	private String houseCondition;
	private String houseRoofType;
	private String houseWall;
	private String houseFloor;
	private String houseElectricity;
	private String houseSourceWater;
	private String houseIndexTotalScore;
	private String totalAset;
	private String loanAset;
	private String netoAset;
	private String formedByPersonnel;
	private String trained;
	private String trainedDate;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCustomerNumberCenter() {
		return customerNumberCenter;
	}

	public void setCustomerNumberCenter(String customerNumberCenter) {
		this.customerNumberCenter = customerNumberCenter;
	}

	public String getCustomerNumberGroup() {
		return customerNumberGroup;
	}

	public void setCustomerNumberGroup(String customerNumberGroup) {
		this.customerNumberGroup = customerNumberGroup;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGovernmentId() {
		return governmentId;
	}

	public void setGovernmentId(String governmentId) {
		this.governmentId = governmentId;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getGroupFlag() {
		return groupFlag;
	}

	public void setGroupFlag(String groupFlag) {
		this.groupFlag = groupFlag;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getDateOfGovernmentId() {
		return dateOfGovernmentId;
	}

	public void setDateOfGovernmentId(String dateOfGovernmentId) {
		this.dateOfGovernmentId = dateOfGovernmentId;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getTelephone() {
		if(this.telephone==null)
			return "";
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getNumberOfChildren() {
		if(this.numberOfChildren==null)
			return "";
		return numberOfChildren;
	}

	public void setNumberOfChildren(String numberOfChildren) {
		this.numberOfChildren = numberOfChildren;
	}

	public String getCitizenShip() {
		if(this.citizenShip==null)
			return "";
		return citizenShip;
	}

	public void setCitizenShip(String citizenShip) {
		this.citizenShip = citizenShip;
	}

	public String getReligion() {
		if(this.religion==null)
			return "";
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getEthinicity() {
		if(this.ethinicity==null)
			return "";
		return ethinicity;
	}

	public void setEthinicity(String ethinicity) {
		this.ethinicity = ethinicity;
	}

	public String getCitizenType() {
		if(this.citizenType==null)
			return "";
		return citizenType;
	}

	public void setCitizenType(String citizenType) {
		this.citizenType = citizenType;
	}

	public String getResidenceStatus() {
		if(this.residenceStatus==null)
			return "";
		return residenceStatus;
	}

	public void setResidenceStatus(String residenceStatus) {
		this.residenceStatus = residenceStatus;
	}

	public String getLatestEducation() {
		return latestEducation;
	}

	public void setLatestEducation(String latestEducation) {
		this.latestEducation = latestEducation;
	}

	public String getHusbandName() {
		return husbandName;
	}

	public void setHusbandName(String husbandName) {
		this.husbandName = husbandName;
	}

	public String getHusbandDateOfBirth() {
		return husbandDateOfBirth;
	}

	public void setHusbandDateOfBirth(String husbandDateOfBirth) {
		this.husbandDateOfBirth = husbandDateOfBirth;
	}

	public String getHusbandPlaceOfBirth() {
		return husbandPlaceOfBirth;
	}

	public void setHusbandPlaceOfBirth(String husbandPlaceOfBirth) {
		this.husbandPlaceOfBirth = husbandPlaceOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRtRw() {
		return rtRw;
	}

	public void setRtRw(String rtRw) {
		this.rtRw = rtRw;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getSavingAccountFlag() {
		return savingAccountFlag;
	}

	public void setSavingAccountFlag(String savingAccountFlag) {
		this.savingAccountFlag = savingAccountFlag;
	}

	public String getReasonOpenAccount() {
		return reasonOpenAccount;
	}

	public void setReasonOpenAccount(String reasonOpenAccount) {
		this.reasonOpenAccount = reasonOpenAccount;
	}

	public String getFundSource() {
		return fundSource;
	}

	public void setFundSource(String fundSource) {
		this.fundSource = fundSource;
	}

	public String getHouseCapacious() {
		if(this.houseCapacious==null)
			return "";
		return houseCapacious;
	}

	public void setHouseCapacious(String houseCapacious) {
		this.houseCapacious = houseCapacious;
	}

	public String getHouseCondition() {
		if(this.houseCondition==null)
			return "";
		return houseCondition;
	}

	public void setHouseCondition(String houseCondition) {
		this.houseCondition = houseCondition;
	}

	public String getHouseRoofType() {
		if(this.houseRoofType==null)
			return "";
		return houseRoofType;
	}

	public void setHouseRoofType(String houseRoofType) {
		this.houseRoofType = houseRoofType;
	}

	public String getHouseWall() {
		if(this.houseWall==null)
			return "";
		return houseWall;
	}

	public void setHouseWall(String houseWall) {
		this.houseWall = houseWall;
	}

	public String getHouseFloor() {
		if(this.houseFloor==null)
			return "";
		return houseFloor;
	}

	public void setHouseFloor(String houseFloor) {
		this.houseFloor = houseFloor;
	}

	public String getHouseElectricity() {
		if(this.houseElectricity==null)
			return "";
		return houseElectricity;
	}

	public void setHouseElectricity(String houseElectricity) {
		this.houseElectricity = houseElectricity;
	}

	public String getHouseSourceWater() {
		if(this.houseSourceWater==null)
			return "";
		return houseSourceWater;
	}

	public void setHouseSourceWater(String houseSourceWater) {
		this.houseSourceWater = houseSourceWater;
	}

	public String getHouseIndexTotalScore() {
		if(this.houseIndexTotalScore==null)
			return "";
		return houseIndexTotalScore;
	}

	public void setHouseIndexTotalScore(String houseIndexTotalScore) {
		this.houseIndexTotalScore = houseIndexTotalScore;
	}

	public String getTotalAset() {
		if(this.totalAset==null)
			return "";
		return totalAset;
	}

	public void setTotalAset(String totalAset) {
		this.totalAset = totalAset;
	}

	public String getLoanAset() {
		if(this.loanAset==null)
			return "";
		return loanAset;
	}

	public void setLoanAset(String loanAset) {
		this.loanAset = loanAset;
	}

	public String getNetoAset() {
		if(this.netoAset==null)
			return "";
		return netoAset;
	}

	public void setNetoAset(String netoAset) {
		this.netoAset = netoAset;
	}

	public String getFormedByPersonnel() {
		return formedByPersonnel;
	}

	public void setFormedByPersonnel(String formedByPersonnel) {
		this.formedByPersonnel = formedByPersonnel;
	}

	public String getTrained() {
		if(trained==null)
			return "";
		return trained;
	}

	public void setTrained(String trained) {
		this.trained = trained;
	}

	public String getTrainedDate() {
		return trainedDate;
	}

	public void setTrainedDate(String trainedDate) {
		this.trainedDate = trainedDate;
	}

	public String getBusinessIncomeFamily() {
		if(this.businessIncomeFamily==null)
			return "";
		return businessIncomeFamily;
	}

	public void setBusinessIncomeFamily(String businessIncomeFamily) {
		this.businessIncomeFamily = businessIncomeFamily;
	}

	@Override
	public String toString() {
		return "CustomerRequest [username=" + username + ", imei=" + imei + ", customerNumberCenter="
				+ customerNumberCenter + ", customerNumberGroup=" + customerNumberGroup + ", actionFlag=" + actionFlag
				+ ", reffId=" + reffId + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", groupFlag="
				+ groupFlag + ", fullName=" + fullName + ", firstName=" + firstName + ", nickName=" + nickName
				+ ", governmentId=" + governmentId + ", dateOfGovernmentId=" + dateOfGovernmentId + ", dateOfBirth="
				+ dateOfBirth + ", placeOfBirth=" + placeOfBirth + ", telephone=" + telephone + ", motherName="
				+ motherName + ", gender=" + gender + ", maritalStatus=" + maritalStatus + ", occupation=" + occupation
				+ ", numberOfChildren=" + numberOfChildren + ", businessIncomeFamily=" + businessIncomeFamily
				+ ", citizenShip=" + citizenShip + ", religion=" + religion + ", ethinicity=" + ethinicity
				+ ", citizenType=" + citizenType + ", residenceStatus=" + residenceStatus + ", latestEducation="
				+ latestEducation + ", husbandName=" + husbandName + ", husbandDateOfBirth=" + husbandDateOfBirth
				+ ", husbandPlaceOfBirth=" + husbandPlaceOfBirth + ", address=" + address + ", rtRw=" + rtRw
				+ ", kelurahan=" + kelurahan + ", kecamatan=" + kecamatan + ", state=" + state + ", country=" + country
				+ ", postalCode=" + postalCode + ", savingAccountFlag=" + savingAccountFlag + ", reasonOpenAccount="
				+ reasonOpenAccount + ", fundSource=" + fundSource + ", houseCapacious=" + houseCapacious
				+ ", houseCondition=" + houseCondition + ", houseRoofType=" + houseRoofType + ", houseWall=" + houseWall
				+ ", houseFloor=" + houseFloor + ", houseElectricity=" + houseElectricity + ", houseSourceWater="
				+ houseSourceWater + ", houseIndexTotalScore=" + houseIndexTotalScore + ", totalAset=" + totalAset
				+ ", loanAset=" + loanAset + ", netoAset=" + netoAset + ", formedByPersonnel=" + formedByPersonnel
				+ ", trained=" + trained + ", trainedDate=" + trainedDate + ", getTransmissionDateAndTime()="
				+ getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
				+ "]";
	}

}
